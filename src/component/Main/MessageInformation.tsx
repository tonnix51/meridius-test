import { useEffect } from "react";

const MessageInformation = ({ message, setSuccess, success }: any) => {

    useEffect(()=>{
        setTimeout(()=>{
            setSuccess(false)
        },3000)
    },[success])

    return (
        <div className={`message_information ${ !success ? "message_height_zero" : "" }`}>
            { message }
        </div>
    );
};

export default MessageInformation;