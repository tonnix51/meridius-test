import { RecoilRoot } from 'recoil';
import './App.css'
import Application from './component/Application';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div>
      <RecoilRoot>
        <Application/>
      </RecoilRoot>
    </div>
  )
}

export default App
