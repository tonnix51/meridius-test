const Footer = () => {
    const footerTools = [
        "Terms & Conditions",
        "Privacy Policy",
        "FAQ",
        "Contact Us"
    ]

    return (
        <div className="footer_style">
            <p><a href="#">Get in touch with our support team</a> if you have any question or want to leave some feedback.
            We’ll be happy to hear from you.</p>

            <div className="footer_tools_style">
                <hr className="hr_style"/>
                <ul>
                    {
                        footerTools.map( (tools, index) => (
                            <li key={index}><a href="#">{tools}</a></li>
                        ))
                    }
                </ul>
            </div>
        </div>
    );
};

export default Footer;