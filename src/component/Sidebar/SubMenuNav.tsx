const SubMenuNav = ({ icon, data }: any) => {
    return (
        <div>
            {
                data.map( (item: any, index: number) => (
                    <div key={index} className="d-flex align-item-center nav_container">
                      {icon}<p><a href="#">{ item }</a></p>
                    </div>
                ))
            }
        </div>
    );
};

export default SubMenuNav;