import { atom } from "recoil";

export const MenuBoard = atom({
    key: "MenuBoard",
    default: true
})