import MainContainer from './Main/MainContainer';
import Sidebar from './Sidebar/Sidebar';

const Application = () => {
    return (
        <div className="d-flex align-item-center">
            <Sidebar/>
            <MainContainer/>
        </div>
    );
};

export default Application;