import PlusIcon from "./PlusIcon";

const MenuNavbar = ({ icon, title, className }: any) => {
    return (
        <div className="menu_nav_title_content">
            <div className={className}>
                { icon }

                <h3>{ title }</h3>
            </div>

            {
                title === "My boards" ? (
                    <PlusIcon/>
                ) : null
            }
        </div>
    );
};

export default MenuNavbar;