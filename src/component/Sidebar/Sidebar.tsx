import Account from "./Account";
import Logo from "./Logo";
import MenuNavbar from "./MenuNavbar";
import SubMenuNav from "./SubMenuNav";
import { useRecoilValue } from "recoil";
import { MenuBoard } from "../../State/MenuState";

const Sidebar = () => {

    const openBoard = useRecoilValue(MenuBoard)

    const favoriteDataNav = [
        "Software Engineer",
        "Computer hardware engineer",
        "Network Engineer",
        "Technical Support",
        "Network administrator",
        "Management",
        "Data analysis",
        "Computer technician"
    ]

    const pastDataNav = [
        "Past search 1",
        "Past search 2",
        "Computers and information...",
        "Database Administrator",
        "Computer security",
        "Computer Systems Analyst"
    ]

    const boardData = [
        "Board 1",
        "Board 2",
        "Board 3"
    ]

    const boardAgent = [
        "Board agent 1",
        "Board agent 1",
        "Board agent 1"
    ]

    return (
        <div className="sidebar_container">
            <Logo/>

            <nav className="nav_style">

                <MenuNavbar icon={
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M1.04063 1.59693C1.01399 1.72713 1 1.86193 1 2V14C1 15.1046 1.89543 16 3 16H13C14.1046 16 15 15.1046 15 14V2C15 0.895431 14.1046 0 13 0H3C2.0335 0 1.22713 0.685564 1.04063 1.59693ZM3 2H13V14H3V2ZM7 6H5V4H7V6ZM8 6H11V5H8V6ZM11 7V8H5V7H11ZM11 10V9H5V10H11ZM9 11V12H5V11H9Z" fill="#4C4C55"/>
                                </svg>
                            } 
                            title="My templates" 
                            className="d-flex align-item-center nav_container notHover"/>

                <MenuNavbar icon={
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clipPath="url(#clip0_1894_297)">
                                <path fillRule="evenodd" clipRule="evenodd" d="M6.5 0C10.0899 0 13 2.91015 13 6.5C13 7.93484 12.5351 9.2611 11.7477 10.3363L15.2932 13.8818C15.6837 14.2723 15.6837 14.9055 15.2932 15.296C14.9026 15.6866 14.2695 15.6866 13.8789 15.296L10.333 11.7501C9.25837 12.536 7.93336 13 6.5 13C2.91015 13 0 10.0899 0 6.5C0 2.91015 2.91015 0 6.5 0ZM6.5 2C8.98528 2 11 4.01472 11 6.5C11 8.98528 8.98528 11 6.5 11C4.01472 11 2 8.98528 2 6.5C2 4.01472 4.01472 2 6.5 2Z" fill="#4C4C55"/>
                                </g>
                                <defs>
                                <clipPath id="clip0_1894_297">
                                <rect width="16" height="16" fill="white"/>
                                </clipPath>
                                </defs>
                                </svg>
                            } 
                            title="Search" 
                            className="d-flex align-item-center nav_container notHover"/>

                <SubMenuNav icon="⭐️" data={favoriteDataNav}/>

                <SubMenuNav icon={
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="1" y="1" width="14" height="14" fill="white"/>
                                <g clipPath="url(#clip0_1904_369)">
                                <path d="M8.00006 4.10229V8.00002H11.8978" stroke="#4C4C55" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                                <path d="M8.00006 14.125C11.3828 14.125 14.1251 11.3827 14.1251 8C14.1251 4.61726 11.3828 1.875 8.00006 1.875C4.61732 1.875 1.87506 4.61726 1.87506 8C1.87506 11.3827 4.61732 14.125 8.00006 14.125Z" stroke="#4C4C55" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                                </g>
                                <defs>
                                <clipPath id="clip0_1904_369">
                                <rect width="14" height="14" fill="white" transform="translate(1 1)"/>
                                </clipPath>
                                </defs>
                            </svg>
                            } data={pastDataNav}/>

                <MenuNavbar icon={
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M1.04063 1.59693C1.01399 1.72713 1 1.86193 1 2V14C1 15.1046 1.89543 16 3 16H13C14.1046 16 15 15.1046 15 14V2C15 0.895431 14.1046 0 13 0H3C2.0335 0 1.22713 0.685564 1.04063 1.59693ZM3 2H13V7H3V2ZM3 9V14H13V9H3ZM6 5H10V4H6V5ZM10 12H6V11H10V12Z" fill="#4C4C55"/>
                                </svg>                                
                            } 
                            title="My boards" 
                            className="d-flex align-item-center nav_container notHover"/>

                <div className={ `board_style ${openBoard ? "" : "height_zero"}`}>
                    <SubMenuNav icon="🗂️" data={boardData}/>

                    <SubMenuNav icon="🔒" data={boardAgent}/>
                </div>
            </nav>

            <Account/>
        </div>
    );
};

export default Sidebar;