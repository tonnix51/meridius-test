import AccountForm from './AccountForm';
import AccountInformation from './AccountInformation';
import DeleteAccount from './DeleteAccount';
import Footer from './Footer';

const MainContainer = () => {
    return (
        <div className="main_container">
            <AccountInformation/>
            <AccountForm/>
            <DeleteAccount/>
            <Footer/>
        </div>
    );
};

export default MainContainer;