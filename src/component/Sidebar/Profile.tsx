const Profile = ({image, name}: any) => {
    return (
        <div className="profile_content">
            <div className="profile_image_content">
                <img src={image} 
                    alt="profile_image" 
                    width={32} 
                    height={32}/>
            </div>

            <p>{name}</p>
        </div>
    );
};

export default Profile;