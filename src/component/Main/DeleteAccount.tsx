const DeleteAccount = () => {
    return (
        <div className="delete_account_container">
            <h5>Delete account</h5>

            <p>If you delete your account you’ll be permanently removing it from our systems - you can’t undo it.</p>

            <span><a href="#">Yes, Delete my account</a></span>
        </div>
    );
};

export default DeleteAccount;