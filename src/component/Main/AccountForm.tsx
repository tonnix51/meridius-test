import InputInForm from "./InputInForm";
import profileImage from "../../assets/profile2_meridius.png"
import MessageInformation from "./MessageInformation";
import { useState } from "react";

const AccountForm = () => {
    const userFields = [
        {
            label: "First Name",
            width: "50%",
            id: "firstname",
            type: "text",
            defaultValue: "Petter"
        },
        {
            label: "Last Name",
            width: "50%",
            id: "lastname",
            type: "text",
            defaultValue: "Cetera"
        },
        {
            label: "City",
            width: "50%",
            id: "city",
            type: "text",
            defaultValue: "London"
        },
        {
            label: "Postal Code",
            width: "50%",
            id: "postalCode",
            type: "text",
            defaultValue: "E2 4XF"
        },
        {
            label: "Address",
            width: "100%",
            id: "address",
            type: "text",
            defaultValue: "123 Example"
        },
        {
            label: "Email",
            width: "50%",
            id: "email",
            type: "email",
            defaultValue: "petter@gmail.com"
        },
        {
            label: "Phone",
            width: "50%",
            id: "phone",
            type: "text",
            defaultValue: "+442223334444"
        },
        {
            label: "Password",
            width: "50%",
            id: "password",
            type: "password"
        },
    ]

    const [success, setSuccess] = useState(false)

    const PostData = async( ev: React.FormEvent<HTMLFormElement> )=>{
        ev.preventDefault()

        const form = ev.target as HTMLFormElement
        const formData = new FormData(form)
        const data = Object.fromEntries(formData)

        try{
            //fetch data
            console.log(data);
            setSuccess(true)
            form.reset()
        }catch (err){
            console.error(err);
        }
    }

    return (
        <div className="account_form_container">
            <h4>Personal Information</h4>

            <MessageInformation message="The user has been successfully added." 
                                setSuccess={setSuccess} 
                                success={success}/>

            <div className="d-flex align-item-start">
                
                <form method="POST" className="form_style" onSubmit={PostData}>
                    <div className="input_container">
                        {
                            userFields.map( (field, index) => (
                                <div key={index} style={{width: field.width}}>
                                    <InputInForm id={field.id} 
                                                label={field.label} 
                                                defaultValue={field.defaultValue}
                                                type={field.type}/>
                                </div>
                            ))
                        }
                    </div>
                    <div>
                        <p className="text_in_form">Use this email to log in to your <a href="#">resumedone.io</a> account and receive notifications.</p>

                        <button className="Button_style" type="submit">
                            Save
                        </button>
                    </div>
                </form>

                <div className="profile_image">
                    <img src={profileImage} 
                        alt="profile_image" 
                        width={144} 
                        height={144}/>
                </div>
            </div>

            <div className="d-flex align-item-center input_show">
                <input type="checkbox" name="show" id="show"/>
                <label htmlFor="show" className="label_for_show">Show my profile to serious employers on <a href="#">hirethesbest.io</a> for free</label>
            </div>
        </div>
    );
};

export default AccountForm;