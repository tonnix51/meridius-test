import AccountInformationIcon from "./AccountInformationIcon";

const AccountInformation = () => {
    return (
        <div className="account_information_container">
            <AccountInformationIcon/>

            <div className="account_information_text">
                <h4>Premium Account</h4>

                <p>You have a premium account, granting you access to all the remarkable features offered by ResumeDone. With this privilege, you can indulge in the freedom of downloading an unlimited number of resumes and cover letters in both PDF and Word formats.</p>
            </div>
        </div>
    );
};

export default AccountInformation;